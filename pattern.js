let result, character, x;

character = "";
result = "";
x = 0;

// pattern 1
// function patternSatu(n = 5)
// {
//     x = n;
//     for (let i = 0; i < n; i++) {
//         character += "*";
//         result += character + "_".repeat(x--) + "<br>";
//     }
//
//     return result;
// }

// pattern 2
// function patternDua(n = 5)
// {
//     x = n;
//     for (let i = 0; i < n; i++) {
//         character += "*";
//         result += "_".repeat(x--) + character + "<br>";
//     }
//
//     return result;
// }

// pattern 3
// function patternTiga(n = 5)
// {
//     x = n;
//     for (let i = n; i > 0; i--)
//     {
//         character = "*".repeat(x--);
//         result += character + "_".repeat(n - i) + "<br>";
//     }
//
//     return result;
// }

// pattern 4
// function patternEmpat(n = 5)
// {
//     x = n;
//     for (let i = n; i > 0; i--)
//     {
//         character = "*".repeat(x--);
//         result += "_".repeat(n - i) + character + "<br>";
//     }
//
//     return result;
// }

// pattern 5
// function patternLima(n = 5) {
//     let space = "", point = 1;
//     x = n - 1;
//
//     for (let i = 0; i < n; i++) {
//             space = "_".repeat(x--);
//             character = "*".repeat(point);
//             result += space+character+space+"<br>";
//             point += 2;
//     }
//
//     return result;
// }

// pattern 6
// function patternEnam(n = 5)
// {
//     let space = "", point = n * 2 - 1;
//     x = 1;
//
//     for (let i = n; i > 0; i--) {
//         space = "_".repeat(x++);
//         character = "*".repeat(point);
//         result += space+character+space+"<br>";
//         point -= 2;
//     }
//
//     return result;
// }

// pattern 7
// function patternTujuh(n = 9)
// {
//     let combine1, combine2, y;
//
//     x = Math.ceil(n / 2);
//     y = Math.floor(n / 2);
//
//     combine1 = patternLima(x);
//     combine1 = "";
//     combine2 = patternEnam(y);
//     result = combine1 + combine2;
//
//     return result;
// }

// pattern 8
// function patternDelapan(n = 9)
// {
//     let combine1, combine2, y;
//
//     x = Math.ceil(n / 2);
//     y = Math.floor(n / 2);
//
//     combine1 = patternSatu(x);
//     combine1 = "";
//     combine2 = patternTiga(y);
//     result = combine1 + combine2;
//
//     return result;
// }

// pattern 9
// function patternSembilan(n = 9)
// {
//     let combine1, combine2, y;
//
//     x = Math.ceil(n / 2);
//     y = Math.floor(n / 2);
//
//     combine1 = patternDua(x);
//     combine1 = "";
//     combine2 = patternEmpat(y);
//     result = combine1 + combine2;
//
//     return result;
// }

// pattern 10
// function patternSepuluh(n = 5)
// {
//     x = n;
//     for (let i = 0; i < n; i++) {
//         character = "*".repeat(n);
//         result += "_".repeat(x--) + character + "<br>";
//     }
//
//     return result;
// }

// pattern 11
// function patternSebelas(n = 5)
// {
//     x = 0;
//     for (let i = n; i > 0; i--) {
//         character = "*".repeat(n);
//         result += "_".repeat(x++) + character + "<br>";
//     }
//
//     return result;
// }

// pattern 12
// function patternDuaBelas(n = 9)
// {
//     let combine1, combine2, y;
//
//     x = Math.ceil(n / 2);
//     y = Math.floor(n / 2);
//
//     combine1 = patternTiga(x);
//     combine1 = "";
//     combine2 = patternSatu(y);
//     result = combine1 + combine2;
//
//     return result;
// }

// pattern 13
// function patternTigaBelas(n = 9)
// {
//     let combine1, combine2, y;
//
//     x = Math.ceil(n / 2);
//     y = Math.floor(n / 2);
//
//     combine1 = patternEmpat(x);
//     combine1 = "";
//     combine2 = patternDua(y);
//     result = combine1 + combine2;
//
//     return result;
// }

/* ========================================================== */

// pattern sub 14
function patternSubEmpatBelas(n = 9) {
    let space = "", point = 1;
    x = n;
    result = [];

    for (let i = 0; i < n; i++) {
        space = "&nbsp;".repeat(x--);
        character = "*".repeat(point);
        result.push(space + character + space + "<br>");
        point += 1;
    }

    return result;
}

// pattern 14
// function patternEmpatBelas(n = 9)
// {
//     let combine1, combine2, y;
//
//     x = Math.ceil(n / 2);
//     y = Math.ceil(n / 2);
//
//
//     combine1 = patternSubEmpatBelas(x).reverse().join('');
//     // combine1 = "";
//     combine2 = patternSubEmpatBelas(y);
//     combine2.splice(0, 1);
//     result = combine1 + combine2.join('');
//
//     return result;
// }
/* ========================================================== */

// pattern 15 & 17
function subLimaBelas(n) {
    let space = "", point = 1;
    x = n;
    result = [];

    for (let i = 0; i < n; i++) {
        space = "_".repeat(x--);

        if (!((i - 1) + 1) || !(i - 1)) {
            character = "*".repeat(point);
        } else if (i === (n - 1)) {
            character = "*".repeat(point);
        } else {
            character = "*" + "_".repeat(point - 2) + "*";
        }

        result.push(character + space + "<br>");
        point += 1;
    }

    return result;
}

// subLimaBelas(5).join('');
// subLimaBelas(5).reverse().join('');


// pattern 16 & 18
function subEnamBelas(n) {
    let space = "", point = 1;
    x = n;
    result = [];

    for (let i = 0; i < n; i++) {
        space = "_".repeat(x--);

        if (!((i - 1) + 1) || !(i - 1)) {
            character = "*".repeat(point);
        } else if (i === (n - 1)) {
            character = "*".repeat(point);
        } else {
            character = "*" + "_".repeat(point - 2) + "*";
        }

        result.push(space + character + "<br>");
        point += 1;
    }

    return result;
}

// subEnamBelas(5).join('');
// subEnamBelas(5).reverse().join('');

// pattern 19 & 20
function subSembilanBelas(n) {
    let space = "", point = 1;
    x = n;
    result = [];

    for (let i = 0; i < n; i++) {
        space = "_".repeat(x--);

        if (!((i - 1) + 1)) {
            character = "*".repeat(point);
        } else if (i === (n - 1)) {
            character = "*".repeat(point);
        } else {
            character = "*" + "_".repeat(point - 2) + "*";
        }

        result.push(space + character + space + "<br>");
        point += 2;
    }

    return result;
}

// subSembilanBelas(5).join('');
// subSembilanBelas(5).reverse().join('');

// pattern 21
function duaSatu(n) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subSembilanBelas(x);
    combine1.splice(x - 1, 1);
    add = '_*' + '_'.repeat(n - 2) + '*_' + '<br>';
    combine1.push(add);

    combine2 = subSembilanBelas(y).reverse();
    combine2.splice(0, 1);

    result = combine1.join('') + combine2.join('');

    return result;
}

duaSatu(9);

// pattern 22
function duaDua(n = 9) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subLimaBelas(x);
    combine1.splice(x - 1, 1);
    add = '*' + '_'.repeat(y - 2) + '*_' + '<br>';
    combine1.push(add);

    combine2 = subLimaBelas(y).reverse();
    combine2.splice(0, 1);

    result = combine1.join('') + combine2.join('');

    return result;
}

// pattern 23
function duaTiga(n = 9) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subEnamBelas(x);
    combine1.splice(x - 1, 1);
    add = '_*' + '_'.repeat(y - 2) + '*' + '<br>';
    combine1.push(add);

    combine2 = subEnamBelas(y).reverse();
    combine2.splice(0, 1);

    result = combine1.join('') + combine2.join('');

    return result;
}

/*==============================================================*/

// sub pattern 26 & pattern 27
function subDuaEnam(n = 5) {
    let result, space, x, y;

    x = (n * 2);
    y = ((n * 2) - x) / 2;

    result = [];

    for (let i = 0; i < n; i++) {
        y++;
        x -= 2;
        space = '*'.repeat(y) + '_'.repeat(x) + '*'.repeat(y) + "<br>";
        result.push(space);
    }

    return result;
}

function duaEnam(n) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subDuaEnam(x);
    combine1.splice(x - 1, 1);

    combine2 = subDuaEnam(y).reverse();

    result = combine1.join('') + combine2.join('');

    return result;
}

function duaTujuh(n = 9) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subDuaEnam(x).reverse();

    combine2 = subDuaEnam(y);

    result = combine1.join('') + combine2.join('');

    return result;
}

/*==============================================================*/

/* ====================================================== */
function subDuaSembilan(n = 9) {
    let space = "", point = 1;
    x = n - 1;
    result = [];

    for (let i = 0; i < n; i++) {
        space = "&nbsp;".repeat(x--);
        if (!((i - 1) + 1) || !(i - 1)) {
            character = "*".repeat(point);
        } else if (i === (n - 1)) {
            character = "*".repeat(point);
        } else {
            character = "*" + "_".repeat(point - 2) + "*";
        }
        result.push(space + character + space + "<br>");
        point += 1;
    }

    return result;
}

// pattern 28
function duaDelapan(n = 9) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subDuaSembilan(x);
    combine1.splice(x, 1);
    add = '*' + '_'.repeat(y - 2) + '*' + '<br>';
    combine1.push(add);

    combine2 = subDuaSembilan(y).reverse();
    combine2.splice(0, 1);

    result = combine1.join('') + combine2.join('');

    return result;
}

// pattern 29
function duaSembilan(n = 9) {
    let combine1, combine2, y;

    x = Math.ceil(n / 2);
    y = Math.ceil(n / 2);

    combine1 = subDuaSembilan(y).reverse();
    combine1.splice(x, 1);
    add = '&nbsp;'.repeat(y - 1) + '*' + '<br>';
    combine1.push(add);

    combine2 = subDuaSembilan(y);
    combine2.splice(0, 1);

    result = combine1.join('') + combine2.join('');

    return result;
}

/* ====================================================== */

// pattern 30
// function tigaPuluh(n = 5)
// {
//     result = [];
//     character = "*".repeat(n);
//     for (let i = 0; i < n; i++) {
//         result.push(character+ "<br>");
//     }
//
//     return result;
// }

// pattern 31
// function tigaSatu(n = 5)
// {
//     let space = "", point = n - 2;
//     x = n;
//     result = [];
//
//     for (let i = 0; i < n; i++) {
//         space = "_".repeat(x--);
//
//         if (!((i - 1) + 1)) {
//             character = "*".repeat(n);
//         } else if (i === (n - 1)) {
//             character = "*".repeat(n);
//         } else {
//             character = "*" + "_".repeat(point) + "*";
//         }
//
//         result.push(character+ "<br>");
//     }
//
//     return result;
// }

// pattern 24 & pattern 25
function duaEmpat(n)
{
    let x, character, result, y;
    x = n - 1;
    y = 0;

    result = [];

    for (let i = 0; i < n; i++) {
        if (!((i - 1) + 1)) {
            character = "_".repeat(x--) + "*".repeat(n) + "_".repeat(y++) + "<br>";
        } else if (i === (n - 1)) {
            character = "_".repeat(x--) + "*".repeat(n) + "_".repeat(y++) + "<br>";
        } else {
            character = "_".repeat(x--) + "*" + "_".repeat(n - 2) + "*" + "_".repeat(y++) + "<br>";
        }
        result.push(character);
    }

    return result;
}

// duaEmpat(5).join('')
// duaEmpat(5).reverse().join('')

document.getElementById("app").innerHTML = duaEmpat(35).reverse().join('');

